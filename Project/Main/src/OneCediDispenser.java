public class OneCediDispenser implements MoneyDispenseChain{
    private MoneyDispenseChain nextDispenser;

    @Override
    public void setNextChain(MoneyDispenseChain nextDispenser) {
        this.nextDispenser = nextDispenser;
    }

    @Override
    public void dispense(Money money) {
        if(money.getAmount() >= 1){
            int numberOfNotes = money.getAmount()/1;
            System.out.println("Dispensing " + numberOfNotes  +" note(s) of 1 cedi");
//            int remainder = money.getAmount() % 1;
//            System.out.println("Dispensing " + numberOfNotes  +" note(s) of 1 cedi");
//            if(remainder != 0){
//                Money remainingMoney = new Money(remainder);
//                nextDispenser.dispense(remainingMoney);
//            }
        }
        else{
            nextDispenser.dispense(money);
        }
    }
}
