public class FiftyCedisDispenser implements MoneyDispenseChain{
    private MoneyDispenseChain nextDispenser;

    @Override
    public void setNextChain(MoneyDispenseChain nextDispenser) {
        this.nextDispenser = nextDispenser;
    }

    @Override
    public void dispense(Money money) {
        if(money.getAmount() >= 50){
            int numberOfNotes = money.getAmount()/50;
            int remainder = money.getAmount() % 50;
            System.out.println("Dispensing " + numberOfNotes  +" note(s) of 50 cedis");
            if(remainder != 0){
                Money remainingMoney = new Money(remainder);
                nextDispenser.dispense(remainingMoney);
            }
        }
        else{
            nextDispenser.dispense(money);
        }
    }
}
