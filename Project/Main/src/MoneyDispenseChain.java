public interface MoneyDispenseChain {
    public abstract void setNextChain(MoneyDispenseChain nextDispenser);
    public abstract void dispense(Money money);
}
