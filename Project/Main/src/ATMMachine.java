public class ATMMachine {
    private MoneyDispenseChain dispenser1;
    private int totalAmount;

    public ATMMachine(int totalAmount){
        this.totalAmount = totalAmount;
        dispenser1 = new TwoHundredDispenser();
        MoneyDispenseChain dispenser2 = new HundredCedisDispenser();
        MoneyDispenseChain dispenser3 = new FiftyCedisDispenser();
        MoneyDispenseChain dispenser4 = new TwentyCedisDispenser();
        MoneyDispenseChain dispenser5 = new TenCedisDispenser();
        MoneyDispenseChain dispenser6 = new FiveCedisDispenser();
        MoneyDispenseChain dispenser7 = new TwoCedisDispenser();
        MoneyDispenseChain dispenser8 = new OneCediDispenser();

        dispenser1.setNextChain(dispenser2);
        dispenser2.setNextChain(dispenser3);
        dispenser3.setNextChain(dispenser4);
        dispenser4.setNextChain(dispenser5);
        dispenser5.setNextChain(dispenser6);
        dispenser6.setNextChain(dispenser7);
        dispenser7.setNextChain(dispenser8);
    }

    public MoneyDispenseChain getDispenser1() {
        return dispenser1;
    }

    public boolean isEmpty(){
        return totalAmount==0;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void deductMoney(int amount){
        int balance = totalAmount-amount;
        this.setTotalAmount(balance);
    }
}
