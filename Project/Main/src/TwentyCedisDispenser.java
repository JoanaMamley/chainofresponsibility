public class TwentyCedisDispenser implements MoneyDispenseChain{
    private MoneyDispenseChain nextDispenser;

    @Override
    public void setNextChain(MoneyDispenseChain nextDispenser) {
        this.nextDispenser = nextDispenser;
    }

    @Override
    public void dispense(Money money) {
        if(money.getAmount() >= 20){
            int numberOfNotes = money.getAmount()/20;
            int remainder = money.getAmount() % 20;
            System.out.println("Dispensing " + numberOfNotes  +" note(s) of 20 cedis");
            if(remainder != 0){
                Money remainingMoney = new Money(remainder);
                nextDispenser.dispense(remainingMoney);
            }
        }
        else{
            nextDispenser.dispense(money);
        }
    }
}
