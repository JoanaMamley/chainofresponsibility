public class TenCedisDispenser implements MoneyDispenseChain{
    private MoneyDispenseChain nextDispenser;
    @Override
    public void setNextChain(MoneyDispenseChain nextDispenser) {
        this.nextDispenser = nextDispenser;
    }

    @Override
    public void dispense(Money money) {
        if(money.getAmount() >= 10){
            int numberOfNotes = money.getAmount()/10;
            int remainder = money.getAmount() % 10;
            System.out.println("Dispensing " + numberOfNotes  +" note(s) of 10 cedis");
            if(remainder != 0){
                Money remainingMoney = new Money(remainder);
                nextDispenser.dispense(remainingMoney);
            }
        }
        else{
            nextDispenser.dispense(money);
        }
    }
}
