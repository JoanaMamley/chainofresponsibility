import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ATMMachine ATM = new ATMMachine(2000);
        Scanner scanner = null;
        while (true) {
            int amount = 0;
            try {
                System.out.println("Please enter amount to dispense:");
                scanner = new Scanner(System.in);
                amount= scanner.nextInt();
                if(amount < 1) {
                    System.out.println("Amount should be a positive number");
                    return;
                }else {
                    if(ATM.getTotalAmount() < amount){
                        System.out.println("Out of service");
                    }
                    else{
                        ATM.getDispenser1().dispense(new Money(amount));
                        ATM.deductMoney(amount);
//                        System.out.println(ATM.getTotalAmount());
                    }
                }
            } catch (Exception e) {
                System.out.println("Value should be an integer");
                continue;
                //e.printStackTrace();
            }
        }
    }
}