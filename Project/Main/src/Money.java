public class Money {
    private int amount;

    public Money(int amount){
        this.amount = amount;
    }

    public int getAmount() {
        return this.amount;
    }
}
